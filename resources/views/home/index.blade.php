@extends('layouts.main', ['title' => 'Home'])

@section('content')
    <div class="container-fluid bg-light text-dark p-5">
        <div class="container bg-light p-5">
            <h1 class="display-4">Selamat Datang</h1>
            <hr>
            <p>Sistem Informasi Manajemen Karyawan (SIM Karyawan) merupakan website untuk melakukan manajemen data Karyawan
                pada sebuah perusahaan.</p>
            <a href="/karyawan" class="btn btn-dark">Kelola Data Karyawan</a>
        </div>
    </div>
@endsection
