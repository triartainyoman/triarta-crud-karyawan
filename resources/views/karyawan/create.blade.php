@extends('layouts.main', ['title' => 'Create Karyawan'])

@section('content')
    <h3 class="text-center">Tambah Data Karyawan</h3>
    <div class="row justify-content-center">
        <div class="col-lg-6 mt-4">
            <form action="/karyawan" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="nama_karyawan" class="form-label">Nama Karyawan</label>
                    <input type="text" class="form-control" id="nama_karyawan" name="nama_karyawan" required>
                </div>
                <div class="mb-3">
                    <label for="no_karyawan" class="form-label">No. Karyawan</label>
                    <input type="text" class="form-control" id="no_karyawan" name="no_karyawan" required>
                </div>
                <div class="mb-3">
                    <label for="no_telp_karyawan" class="form-label">No. Telepon</label>
                    <input type="text" class="form-control" id="no_telp_karyawan" name="no_telp_karyawan" required>
                </div>
                <div class="mb-3">
                    <label for="jabatan_karyawan" class="form-label">Jabatan Karyawan</label>
                    <input type="text" class="form-control" id="jabatan_karyawan" name="jabatan_karyawan" required>
                </div>
                <div class="mb-3">
                    <label for="divisi_karyawan" class="form-label">Divisi Karyawan</label>
                    <input type="text" class="form-control" id="divisi_karyawan" name="divisi_karyawan" required>
                </div>
                <div class="mt-4">
                    <button type="submit" class="btn btn-dark">Tambah Data</button>
                    <a href="/karyawan" class="btn btn-outline-dark">Batalkan</a>
                </div>
            </form>
        </div>
    </div>
@endsection
