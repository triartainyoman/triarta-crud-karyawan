@extends('layouts.main', ['title' => 'About'])

@section('content')
    <div class="row justify-content-center align-items-center">
        <div class="col-lg-4">
            <img src="/profile.jpg" class="img-fluid w-75 rounded shadow-sm p-3 mb-5 mx-auto d-block" alt="">
        </div>
        <div class="col-lg-6 p-5">
            <h2>I Nyoman Triarta</h2>
            <h5>1915091045 - SI 5A</h5>
            <p>Website ini dibuat untuk memenuhi tugas 4 mata kuliah Pemrograman Web Berbasis Framework.</p>
        </div>
    </div>
@endsection
