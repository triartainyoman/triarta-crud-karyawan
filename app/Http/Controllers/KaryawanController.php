<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KaryawanController extends Controller
{
    public function index()
    {
        $data_karyawan = DB::table('karyawan')->get();
        return view('karyawan.index', ['data_karyawan' => $data_karyawan]);
    }

    public function create()
    {
        return view('karyawan.create');
    }

    public function store(Request $request)
    {
        $nama_karyawan = $request->nama_karyawan;
        $no_karyawan = $request->no_karyawan;
        $no_telp_karyawan = $request->no_telp_karyawan;
        $jabatan_karyawan = $request->jabatan_karyawan;
        $divisi_karyawan = $request->divisi_karyawan;

        DB::table('karyawan')->insert([
            'nama_karyawan' => $nama_karyawan,
            'no_karyawan' => $no_karyawan,
            'no_telp_karyawan' => $no_telp_karyawan,
            'jabatan_karyawan' => $jabatan_karyawan,
            'divisi_karyawan' => $divisi_karyawan,
        ]);

        return redirect('/karyawan')->with('success', 'Data Karyawan Berhasil Ditambahkan');
    }

    public function edit($id)
    {
        $karyawan = DB::table('karyawan')->find($id);
        return view('karyawan.edit', ['karyawan' => $karyawan]);
    }

    public function update(Request $request, $id)
    {
        $nama_karyawan = $request->nama_karyawan;
        $no_karyawan = $request->no_karyawan;
        $no_telp_karyawan = $request->no_telp_karyawan;
        $jabatan_karyawan = $request->jabatan_karyawan;
        $divisi_karyawan = $request->divisi_karyawan;

        DB::table('karyawan')->where('id', $id)->update([
            'nama_karyawan' => $nama_karyawan,
            'no_karyawan' => $no_karyawan,
            'no_telp_karyawan' => $no_telp_karyawan,
            'jabatan_karyawan' => $jabatan_karyawan,
            'divisi_karyawan' => $divisi_karyawan,
        ]);

        return redirect('/karyawan')->with('success', 'Data Karyawan Berhasil Diubah!');
    }

    public function destroy($id)
    {
        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('/karyawan')->with('success', 'Data Karyawan Berhasil Dihapus!');
    }
}
